namespace StaffServer.Models
{
    using System.Data.Entity;

    public class DBContext : DbContext
    {
        public DBContext()
            : base("name=DBContext")
        {
        }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Company> Companys { get; set; }
    }
}