﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace StaffServer.Models
{
    public class Company
    {
        public int Id { get; set; }
        [MaxLength(50)]
        [Required]
        public string Name { get; set; }
        public virtual HashSet<Employee> Employees { get; set; }
    }
}