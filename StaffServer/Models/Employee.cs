﻿using System.ComponentModel.DataAnnotations;

namespace StaffServer.Models
{
    public class Employee
    {
        public int Id { get; set; }
        [MaxLength(50)]
        [Required]
        public string Name { get; set; }
        [MaxLength(50)]
        [Required]
        public string Surname { get; set; }
        [MaxLength(50)]
        [Required]
        public string Patronymic { get; set; }
        [MaxLength(11)]
        [RegularExpression("[0-9]{11}")]
        public string Phone { get; set; }
        [Required]
        public Company Company { get; set; }
    }
}