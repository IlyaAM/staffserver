﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace StaffServer.Models
{
    public static class DataBase
    {
        public static void AddCompany(Company company)
        {
            SetQuery(dbContext => dbContext.Companys.Add(company));
        }
        public static void ChangeCompany(Company company)
        {
            SetQuery(dbContext => dbContext.Companys
                .Find(company.Id)
                .Name = company.Name);
        }
        public static void AddEmployee(Employee employee, int companyId)
        {
            SetQuery(dbContext =>
            {
                employee.Company = dbContext.Companys.Find(companyId);
                dbContext.Employees.Add(employee);
            });
        }
        public static void ChangeEmployee(Employee employee)
        {
            SetQuery(dbContext => {
                var oldEmploye = dbContext.Employees.Find(employee.Id);
                oldEmploye.Name = employee.Name;
                oldEmploye.Patronymic = employee.Patronymic;
                oldEmploye.Surname = employee.Surname;
                oldEmploye.Phone = employee.Phone;
                oldEmploye.Company = dbContext.Companys.Find(employee.Company.Id);
            });
        }
        private static void SetQuery(Action<DBContext> query)
        {
            using (var dbContext = new DBContext())
            {
                query(dbContext);
                dbContext.SaveChanges();
            }
        }
        public static void RemuveEmployee(int id)
        {
            SetQuery(dbContext => dbContext.Employees
                .Remove(dbContext.Employees
                    .Find(id)));
        }
        public static void RemuveCompany(int id)
        {
            SetQuery(dbContext => dbContext.Companys
                .Remove(dbContext.Companys
                    .Find(id)));
        }
        private static T GetQuery<T>(Func<DBContext, T> query)
        {
            using (var dbContext = new DBContext())
            {
                return query(dbContext);
            }
        }
        public static List<Employee> GetEmployees()
        {
            return GetQuery(dbContext => dbContext.Employees
                .Include(e => e.Company)
                .OrderBy(e => e.Surname)
                .ThenBy(e => e.Name)
                .ThenBy(e => e.Patronymic)
                .ToList());
        }
        public static List<Employee> GetEmployees(int companyId)
        {
            return GetQuery(dbContext => dbContext.Employees
                .Include(e => e.Company)
                .Where(e => e.Company.Id == companyId)
                .OrderBy(e => e.Surname)
                .ThenBy(e => e.Name)
                .ThenBy(e => e.Patronymic)
                .ToList());
        }
        public static Employee GetEmployee(int id)
        {
            return GetQuery(dbContext =>dbContext.Employees
                .Include(e => e.Company)
                .FirstOrDefault(e => e.Id == id));
        }
        public static Company GetCompany(int id)
        {
            return GetQuery(dbContext => dbContext.Companys
                .Include(c => c.Employees)
                .FirstOrDefault(c => c.Id == id));
        }
        public static List<Company> GetCompanies()
        {
            return GetQuery(dbContext => dbContext.Companys
                .Include(c => c.Employees)
                .OrderBy(c => c.Name)
                .ToList());
        }
    }
}