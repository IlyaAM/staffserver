﻿using NUnit.Framework;
using System.Data.Entity;

namespace StaffServer.Models
{
    [TestFixture]
    public class DataBaseTest
	{
        [SetUp]
        public void AddToDatabase()
        {      
            Database.SetInitializer(new DbInitializer());
        }
        [Test]
        public void Initializar()
        {
            DataBase.GetCompanies();
        }
        [Test]
        public void GetEmployees()
        {
            var employees = DataBase.GetEmployees();
            Assert.AreEqual(2, employees.Count);
            Assert.AreEqual("Sergey", employees[0].Name);
            Assert.AreEqual("Lev", employees[1].Name);
        }
        [Test]
        public void GetEmployeesOfCompany()
        {
            var employees = DataBase.GetEmployees(1);
            Assert.AreEqual(2, employees.Count);
            Assert.AreEqual("Sergey", employees[0].Name);
            Assert.AreEqual("Lev", employees[1].Name);
        }
        [Test]
        public void GetEmployee()
        {
            var employee = DataBase.GetEmployee(1);
            Assert.AreEqual("Sergey", employee.Name);
            Assert.AreEqual("Pushkin", employee.Surname);
            Assert.AreEqual("Sergeevich", employee.Patronymic);
            Assert.AreEqual("89634532738", employee.Phone);
            Assert.AreEqual("Googl", employee.Company.Name);

        }
        [Test]
        public void ChangeEmployee()
        {
            var employee = DataBase.GetEmployees()[0];
            employee.Phone = "89999999999";
            DataBase.ChangeEmployee(employee);
            employee = DataBase.GetEmployees()[0];
            Assert.AreEqual("89999999999", employee.Phone);
            employee.Phone = "89634532738";
            DataBase.ChangeEmployee(employee);
        }
        [Test]
        public void RemuveEmployee()
        {
            var deletedEmployee = DataBase.GetEmployee(2);
            DataBase.RemuveEmployee(2);
            var employees = DataBase.GetEmployees();
            Assert.AreEqual(1, employees.Count);
            Assert.AreEqual("Sergey", employees[0].Name);
            DataBase.AddEmployee(deletedEmployee, 1);
        }
        [Test]
        public void GetCompanies()
        {
            var companies = DataBase.GetCompanies();
            Assert.AreEqual(2, companies.Count);
            Assert.AreEqual("Googl", companies[0].Name);
            Assert.AreEqual("Yandex", companies[1].Name);
        }
        [Test]
        public void GetCompany()
        {
            var company = DataBase.GetCompany(1);
            Assert.AreEqual("Googl", company.Name);
        }
        [Test]
        public void ChangeCompany()
        {
            var company = DataBase.GetCompanies()[0];
            company.Name = "GOOGL!";
            DataBase.ChangeCompany(company);
            company = DataBase.GetCompanies()[0];
            Assert.AreEqual("GOOGL!", company.Name);
            company.Name = "Googl";
            DataBase.ChangeCompany(company);
        }
        [Test]
        public void RemuveCompany()
        {
            var deletedCompany = DataBase.GetCompany(2);
            DataBase.RemuveCompany(2);
            var companies = DataBase.GetCompanies();
            Assert.AreEqual(1, companies.Count);
            Assert.AreEqual("Googl", companies[0].Name);
            DataBase.AddCompany(deletedCompany);
        }
    }
}