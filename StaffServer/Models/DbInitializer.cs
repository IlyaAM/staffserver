﻿using System.Data.Entity;

namespace StaffServer.Models
{
    public class DbInitializer : DropCreateDatabaseAlways<DBContext>
    {
        protected override void Seed(DBContext context)
        {
            Company googl = new Company { Name = "Googl" };
            Company yandex = new Company { Name = "Yandex" };          
            var pushkin = new Employee
            {
                Name = "Sergey",
                Patronymic = "Sergeevich",
                Surname = "Pushkin",
                Phone = "89634532738",
                Company = googl
            };
            var tolstoy = new Employee
            {
                Name = "Lev",
                Patronymic = "Nikolayevich",
                Surname = "Tolstoy",
                Phone = "89027462839",
                Company = googl
            };
            context.Companys.Add(googl);
            context.Companys.Add(yandex);
            context.Employees.Add(pushkin);
            context.Employees.Add(tolstoy);
            base.Seed(context);
        }
    }
}