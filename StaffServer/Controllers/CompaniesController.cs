﻿using StaffServer.Models;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;

namespace StaffServer.Controllers
{
    public class CompaniesController : Controller
    {
        public ActionResult Companies()
        {
            return View(CompaniesInitialization(ViewBag));
        }
        public ActionResult AddCompany()
        {
            ViewBag.Title = "Add Company";
            ViewBag.Button = "Add";
            return View();
        }
        public ActionResult ChangeCompany(int? companyId)
        {
            if(companyId == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            ViewBag.Title = "Change Company";
            ViewBag.Button = "Change";
            return View("AddCompany", DataBase.GetCompany(companyId.Value));
        }
        [HttpPost]
        public ActionResult ChangeCompany(Company company)
        {
            if (company.Name.IsNullOrWhiteSpace())
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            DataBase.ChangeCompany(company);
            return View("Companies", CompaniesInitialization(ViewBag));
        }
        [HttpPost]
        public ActionResult AddCompany(Company company)
        {
            if(company.Name.IsNullOrWhiteSpace())
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            DataBase.AddCompany(company);
            return View("Companies", CompaniesInitialization(ViewBag));
        }  
        public ActionResult RemuveCompany(int? companyId)
        {
            if (companyId == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            DataBase.RemuveCompany(companyId.Value);
            return View("Companies", CompaniesInitialization(ViewBag));
        }
        [NonAction]
        public static List<Company> CompaniesInitialization(dynamic viewBag)
        {
            viewBag.Title = "Companies";
            return DataBase.GetCompanies();
        }
    }
}