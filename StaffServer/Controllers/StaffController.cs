﻿using StaffServer.Models;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;

namespace StaffServer.Controllers
{
    public class StaffController : Controller
    {
        public ActionResult Staff(int? companyId)
        {
            return View(StaffInitialization(companyId));
        }
        public ActionResult ChangeEmployee(int? employeeId, int? companyId)
        {
            if (employeeId == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            ViewBag.Title = "Employee";
            ViewBag.Button = "Change";
            ViewBag.companyId = companyId;
            ViewData["companies"] = DataBase.GetCompanies();
            return View("AddEmployee", DataBase.GetEmployee(employeeId.Value));
        }
        [HttpPost]
        public ActionResult ChangeEmployee(Employee employee, int? companyId)
        {
            if (employee.Name.IsNullOrWhiteSpace() || employee.Patronymic.IsNullOrWhiteSpace() ||
                employee.Surname.IsNullOrWhiteSpace() || companyId == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            employee.Company = new Company { Id = companyId.Value };
            DataBase.ChangeEmployee(employee);
            return View("Staff", StaffInitialization(companyId));
        }
        public ActionResult AddEmployee(int? companyId)
        {
            ViewBag.Title = "Add employee";
            ViewBag.Button = "Add";
            ViewBag.companyId = companyId;
            ViewData["companies"] = DataBase.GetCompanies();
            return View();
        }
        [HttpPost]
        public ActionResult AddEmployee(Employee employee, int? companyId)
        {
            if (employee.Name.IsNullOrWhiteSpace() || employee.Patronymic.IsNullOrWhiteSpace() ||
                employee.Surname.IsNullOrWhiteSpace() || companyId == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            DataBase.AddEmployee(employee, companyId.Value);
            return View("Staff", StaffInitialization(companyId));
        }
        private List<Employee> StaffInitialization(int? companyId)
        {
            if (companyId == null)
            {
                ViewBag.Title = "All personnel";
                return DataBase.GetEmployees();
            }
            else
            {
                ViewBag.Title = "Staff of " + DataBase.GetCompany(companyId.Value).Name;
                ViewBag.companyId = companyId;
            }
            return DataBase.GetEmployees(companyId.Value);
        }
        public ActionResult RemyveEmployee(int? employeeId, int? companyId)
        {
            if (employeeId == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            DataBase.RemuveEmployee(employeeId.Value);
            return View("Staff", StaffInitialization(companyId));
        }

        public ActionResult AllPersonnel()
        {
            return View("Staff", StaffInitialization(null));
        }
    }
}